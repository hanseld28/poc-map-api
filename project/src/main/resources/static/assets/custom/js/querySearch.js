$(function() {

    $('#querySearch').click(function() {
        const queryString = $('#inputQueryString').val();
        const queryStringJson = JSON.stringify({ queryString });

        console.log(queryStringJson);

        $.ajax({
            method: 'POST',
            url: 'http://localhost:8080/mapa/enderecos/busca/nomes',
            data: queryStringJson,
            dataType: 'json',
            contentType: 'application/json',
            success: function (adresses) {
                let foundAdressesLength = adresses.length;

                if (adresses && foundAdressesLength > 0) {
                    $('#foundAdressesLength').html(`${foundAdressesLength} endereço(s) encontrado(s)`)

                    leafletMap.setView([adresses[0].latitude, adresses[0].longitude], 10);

                    let i = 0, length = adresses.length;

                    for (; i < length; i++) {
                        const { displayName, latitude, longitude } = adresses[i];
                        const marker = addMarkerOnMap(latitude, longitude, true);
                        marker.bindPopup(`Endereço encontrado: ${displayName}`);
                        marker.openPopup();
                    }
                } else {
                    $('#foundAdressesLength').html('Nenhum endereço encontrado...');
                }

            },
            error: function (err) {
                console.log(err);
            }
        });
    })
});