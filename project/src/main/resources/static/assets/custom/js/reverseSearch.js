$(function() {

    $('#reverseSearch').click(function() {
        const lat = Number.parseFloat($('#inputLatitude').val());
        const lon = Number.parseFloat($('#inputLongitude').val());

        leafletMap.setView([lat, lon], 8);
        const targetMarker = addMarkerOnMap(lat, lon, true);
        targetMarker.bindPopup(`Ponto localizado (latitude: ${lat}; longitude: ${lon})`);
        targetMarker.openPopup();
        const reverseSearchCoordinatesJson = JSON.stringify({ lat, lon });

        console.log(reverseSearchCoordinatesJson);

        $.ajax({
            method: 'POST',
            url: 'http://localhost:8080/mapa/enderecos/busca/coordenadas',
            data: reverseSearchCoordinatesJson,
            dataType: 'json',
            contentType: 'application/json',
            success: function (response) {
                console.log(response);
            },
            error: function (err) {
                console.log(err);
            }
        });
    })
});