$(function () {

    $('#inputQueryString').keyup(async function (event) {
       const searchingString = event.target.value.toLowerCase();
       await doTypingSearchRequest(searchingString);
    });

    function renderFoundAdressesOnResponseList(foundAdresses) {
        const listElement = $('#typingSearchResponse');
        listElement.html('');

        let i = 0, length = foundAdresses.length;
        for (; i < length; i++) {
            const { coordinates, inlineString } = foundAdresses[i];

            let addressLiElement = $(document.createElement('li'));
            addressLiElement.attr('class', `foundAddress`);
            addressLiElement.html(inlineString);
            addressLiElement.click(function () {
                const lat = coordinates[0];
                const lon = coordinates[1];
                const marker = addMarkerOnMap(lat, lon, true);
                marker.bindPopup(`Endereço encontrado: ${inlineString}`);
                marker.openPopup();

                console.log(lat, lon)

                leafletMap.setView([lat, lon], 10);
            }.bind(coordinates));

            listElement.append(addressLiElement);
        }
    }

    async function doTypingSearchRequest(query) {
        const url = 'http://localhost:8080/mapa/typingSearch';
        await $.ajax({
            method: 'GET',
            url: url,
            data: {
                q: query,
                limit: 5,
            },
            responseType: 'application/json',
            success: async function ({ features }) {
                const basicAdressesData = [];
                let i = 0, length = features.length;

                for (; i < length; i++) {
                    let addressFeature = features[i];
                    const { coordinates } = addressFeature.geometry;
                    const { name, state, country, postcode } = addressFeature.properties;
                    const inlineString =
                        `${name}${(postcode) ? `, ${postcode}` : ''} - ${state}, ${country}`;

                    const address = {
                        coordinates,
                        inlineString
                    }

                    basicAdressesData.push(address);
                }

                renderFoundAdressesOnResponseList(basicAdressesData);
            },
            error: function (err) {
                console.error(err);
            }
        });
    }

    $('.foundAddress').click(function (event) {
        const addressClicked = event;
        console.log(addressClicked);
    })

});