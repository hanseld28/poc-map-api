$(function() {
    const buttonCalculateDistanceBetweenTwoPoints = $('#calculateDistanceBetweenTwoPoints');
    const inputLatFrom = $('#inputLatitudeFrom');
    const inputLonFrom = $('#inputLongitudeFrom');
    const inputLatTo = $('#inputLatitudeTo');
    const inputLonTo = $('#inputLongitudeTo');

    $(buttonCalculateDistanceBetweenTwoPoints).click(function() {
        const latFrom = Number.parseFloat($(inputLatFrom).val());
        const lonFrom = Number.parseFloat($(inputLonFrom).val());
        const latTo = Number.parseFloat($(inputLatTo).val());
        const lonTo = Number.parseFloat($(inputLonTo).val());

        const distanceCalc = {
            pointA: {
                latitude: latFrom,
                longitude: lonFrom
            },
            pointB: {
                latitude: latTo,
                longitude: lonTo
            }
        };

        const distanceCalcJson = JSON.stringify(distanceCalc);

        console.log(distanceCalcJson);

        $.ajax({
            method: 'POST',
            url: 'http://localhost:8080/mapa/rota/distancia',
            data: distanceCalcJson,
            dataType: 'json',
            contentType: 'application/json',
            success: function (response) {
                const { distance } = response;
                let message;

                if (distance !== null && distance !== 0) {
                    message = `<h4>Distância: ${(distance / 1000)}km</h4>`;
                } else {
                    message = '<h4>Ocorreu um erro. Não foi possível calcular a distância entre os pontos...</h4>';
                }

                $('#queryResponse').html(message);
            },
            error: function (err) {
                console.log(err);
            }
        });

    });
});