const currentLocation = {
    lat: -12.017830337768213,
    lon: -51.13037109375
};

let leafletMap = {};
const featureGroup = L.featureGroup();
const markers = {};
const draggableMarkers = {};
let currentZoom = 6;
let driversLocation = {};
let circleRange = null;
let stepToSetPoints = 0;
let enabledMarkerRemoving = false;

const truckIcon = L.icon({
    iconUrl:      'assets/icons/colored_truck.png',
    iconSize:     [30, 19],
    iconAnchor:   [14, 18],
    popupAnchor:  [-3, -18]
});

const btnLoadMarkers = $('#btnLoadMarkersOnMap');
const btnRemoveAllMarkers = $('#btnRemoveAllMarkersFromMap');
const rangeRadiusControlElement = document.getElementById('rngRadiusControl');

async function findDrivers() {
    await $.get('http://localhost:8080/motoristas', function (driversLocationJSON) {
        driversLocation = driversLocationJSON;
    });
}

async function getCurrentLocation() {
    await navigator.geolocation.getCurrentPosition(function(position) {
        const { latitude, longitude } = position.coords;
        currentLocation.lat = latitude;
        currentLocation.lon = longitude;
    });
}

async function createMap({ lat, lon }) {
    let myMap = await L.map('mapid', {
        center: [lat, lon],
        layers: [
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            }),
            featureGroup
        ],
        zoom: currentZoom,
        //...photonOptions
    });

    return myMap;
}

function addMarkerOnMap(lat, lon, defaultIcon = false, draggable = false) {
    const latLon = [lat, lon];
    const options = (!defaultIcon)
        ? { icon: truckIcon, draggable: draggable }
        : { draggable: draggable };

    const marker = new L.Marker(latLon, options);

    marker.addEventListener('click', removeMakerLayerById);
    marker.addTo(featureGroup);

    return marker;
}

function addCircleOnMap(lat, lon, radius = 1000) {
    const circleRadius = new L.Circle([lat, lon], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5,
        radius: radius
    }).addTo(leafletMap);

    return circleRadius;
}

function addPopupOnMap(lat, lon, text) {
    L.popup()
        .setLatLng([lat, lon])
        .setContent(text)
        .openOn(leafletMap);
}

function loadMarkersToDriversOnMap(draggable = false) {
    let i = 0, length = driversLocation.length;
    for (; i < length; i++) {
        const { driver, lat, lon } = driversLocation[i];
        const marker = addMarkerOnMap(lat, lon);
        marker.bindPopup(`Motorista: ${driver}`);
        markers[i] = marker;
        const draggable = new L.Draggable(marker);
        draggableMarkers[i] = draggable;
    }
}

function removeAllMarkersFromMap() {
    let layers = featureGroup.getLayers();
    let i = layers.length - 1;

    for (; i > -1; i--) {
        featureGroup.removeLayer(layers[i]);
    }
}

$('#enableDragging').change(function () {
    let layers = featureGroup.getLayers();
    let i = layers.length - 1;

    if ($(this).prop('checked')) {
        for (; i > -1; i--) {
            layers[i].dragging.enable();
        }
    } else {
        for (; i > -1; i--) {
            layers[i].dragging.disable();
        }
    }
});

$('#enableMarkerRemoving').change(function () {
    enabledMarkerRemoving = $(this).prop('checked');

    let layers = featureGroup.getLayers();
    let i = layers.length - 1;

    if ($(this).prop('checked')) {
        for (; i > -1; i--) {
            layers[i].setOpacity(.6)
        }
    } else {
        for (; i > -1; i--) {
            layers[i].setOpacity(1)
        }
    }
});

function drawRadiusOnTheMapWithCurrentValues(radiusValue = null) {
    const radiusValueFromInput = radiusValue !== null ? radiusValue : $('#rngRadiusControl').value;
    let radius = Number.parseInt(radiusValueFromInput) * 10;
    const { lat, lon } = currentLocation;

    if (circleRange === null) {
        circleRange = addCircleOnMap(lat, lon, 5000);
    }

    circleRange.setRadius(radius);
};

function initializeRelatedMapEvents() {
    rangeRadiusControlElement.oninput = function (event) {
        const radius = event.target.valueAsNumber;
        drawRadiusOnTheMapWithCurrentValues(radius);
    }

    setTimeout(function () {
        leafletMap.setView([currentLocation.lat, currentLocation.lon], 10);
        drawRadiusOnTheMapWithCurrentValues();
    }, 2000);

    leafletMap.on('zoom', function (event) {
        currentZoom = this.getZoom();
        let zoomMessage = `${currentZoom}x`;
        $('#currentMapZoom').html(zoomMessage);
    });


    
    btnLoadMarkers.click(function () {
        loadMarkersToDriversOnMap();
        btnLoadMarkers.attr('disabled', true);
        btnRemoveAllMarkers.attr('disabled', false);
    });
    btnRemoveAllMarkers.click(function () {
        removeAllMarkersFromMap();
        btnRemoveAllMarkers.attr('disabled', true);
        btnLoadMarkers.attr('disabled', false);
    });

    leafletMap.on('click', function (event) {
        const {lat, lng} = event.latlng;

        if (stepToSetPoints === 1) {
            $('#inputLatitudeFrom').val(lat);
            $('#inputLongitudeFrom').val(lng);
        } else {
            $('#inputLatitudeTo').val(lat);
            $('#inputLongitudeTo').val(lng);
        }

        let format = 'json';
        const fullUrl = `https://nominatim.openstreetmap.org/reverse?format=${format}&lat=${lat}&lon=${lng}`;

        const ajaxOptions = {
            type: 'GET',
            url: fullUrl,
            dataType: 'json',
            success: function (data) {
                const {display_name, lat, lon, place_id, osm_id, osm_type} = data;
                $('#queryResponse')
                    .html(
                        `
                         <h4>Ponto Clicado no Mapa</h4>
                         <span>Display name:</span> <small>${display_name}</small> 
                         <br/><span>Latitude:</span> <small>${lat}</small> 
                         <br/><span>Longitude:</span> <small>${lon}</small>
                         <br/><span>Place ID:</span> <small>${place_id}</small>
                         <br/><span>OSM ID:</span> <small>${osm_id}</small>
                         <br/><span>OSM type:</span> <small>${osm_type}</small>
                         `);
            },
            error: function (error) {
                console.log('error', error);
            },
        }
        $.ajax(ajaxOptions);
    })
}

$(async function () {
    await findDrivers();
    await getCurrentLocation();
    leafletMap = await createMap(currentLocation);

    initializeRelatedMapEvents();
});

function removeMakerLayerById(event) {
    if (!enabledMarkerRemoving) {
        return;
    }
    const marker = event.target;
    const markerLayerId = featureGroup.getLayerId(marker);
    featureGroup.removeLayer(markerLayerId);
}