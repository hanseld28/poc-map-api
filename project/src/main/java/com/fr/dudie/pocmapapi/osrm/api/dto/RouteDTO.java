package com.fr.dudie.pocmapapi.osrm.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RouteDTO {

    private List<LegDTO> legs;
    private String weightName;
    private Double weight;
    private Double distance;
    private Double duration;
}
