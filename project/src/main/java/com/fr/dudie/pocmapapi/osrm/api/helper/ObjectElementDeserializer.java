package com.fr.dudie.pocmapapi.osrm.api.helper;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import fr.dudie.nominatim.model.Element;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;

public class ObjectElementDeserializer implements JsonDeserializer<Element[]> {

    public ObjectElementDeserializer() {

    }

    public Element[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
        if (!json.isJsonObject()) {
            throw new JsonParseException("Unexpected data: " + json.toString());
        } else {
            Element[] elements = new Element[json.getAsJsonObject().entrySet().size()];
            int i = 0;
            Iterator iterator = json.getAsJsonObject().entrySet().iterator();
            for(; iterator.hasNext(); ++i) {
                Map.Entry<String, JsonElement> elem = (Map.Entry)iterator.next();
                elements[i] = new Element();
                elements[i].setKey((String)elem.getKey());
                elements[i].setValue(((JsonElement)elem.getValue()).getAsString());
            }

            return elements;
        }
    }
}
