package com.fr.dudie.pocmapapi.driverMock.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverMock {

    private String driver;
    private Double lat;
    private Double lon;
}
