package com.fr.dudie.pocmapapi.driverMock.controller;

import com.fr.dudie.pocmapapi.driverMock.helper.DriverMockHelper;
import com.fr.dudie.pocmapapi.driverMock.model.DriverMock;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("motoristas")
public class DriverMockController {

    @GetMapping
    public List<DriverMock> findAllDrivers() {
        return DriverMockHelper.getInstance().findAllDrivers();
    }
}
