package com.fr.dudie.pocmapapi.osrm.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class LegDTO {

    private List<Object> steps;
    private Double weight;
    private Double distance;
    private String summary;
    private Double duration;
}
