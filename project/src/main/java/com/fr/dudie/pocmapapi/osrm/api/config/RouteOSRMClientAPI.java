package com.fr.dudie.pocmapapi.osrm.api.config;

import com.fr.dudie.pocmapapi.osrm.api.dto.CoordinatesDTO;
import com.fr.dudie.pocmapapi.osrm.api.dto.RouteCalcFullResponseDTO;
import com.fr.dudie.pocmapapi.osrm.api.handler.OSRMResponseHandler;
import com.fr.dudie.pocmapapi.osrm.api.helper.ObjectElementDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.dudie.nominatim.model.Element;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Optional;

@Slf4j
public class RouteOSRMClientAPI implements OSRMClientAPI {
    private static RouteOSRMClientAPI instance;
    private static final Logger LOGGER = LoggerFactory.getLogger(RouteOSRMClientAPI.class);
    private static final String ROUTE_SERVICE_DRIVING_URL = "route/v1/driving/";
    public static final String ENCODING_UTF_8 = "UTF-8";
    private final Gson gsonInstance;
    private final HttpClient httpClient;
    private OSRMResponseHandler<RouteCalcFullResponseDTO> defaultSearchResponseHandler;

    private RouteOSRMClientAPI(HttpClient httpClient, Gson gsonInstance) {
        this.httpClient = httpClient;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Element.class, new ObjectElementDeserializer());
        this.gsonInstance = gsonBuilder.create();
        this.defaultSearchResponseHandler = new OSRMResponseHandler<>(this.gsonInstance, (new TypeToken<RouteCalcFullResponseDTO>() {
        }).getType());
    }

    public static RouteOSRMClientAPI getInstance() {
        if (instance == null) {
            instance = new RouteOSRMClientAPI(HttpClients.createDefault(), new Gson());
        }

        return instance;
    }

    public Optional<RouteCalcFullResponseDTO> getDistanceBetweenTwoPoints(CoordinatesDTO pointA, CoordinatesDTO pointB) {
        Optional<RouteCalcFullResponseDTO> optionalRouteCalcFullResponseDTO = null;

        try {
            String url = buildUrlToCoordinatesSearch(pointA, pointB);
            System.out.println(url);
            URIBuilder uriBuilder = new URIBuilder(url);
            URI uri = uriBuilder.build();
            HttpGet getRequest = new HttpGet(uri);
            getRequest.addHeader("accept", "application/json");
            RouteCalcFullResponseDTO routeCalcFullResponseDTO = httpClient.execute(getRequest, this.defaultSearchResponseHandler);

            optionalRouteCalcFullResponseDTO = Optional.of(routeCalcFullResponseDTO);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return optionalRouteCalcFullResponseDTO;
    }

    private String buildUrlToCoordinatesSearch(CoordinatesDTO ...points) {
        StringBuilder buildedUrl = new StringBuilder(DEFAULT_BASE_URL).append(ROUTE_SERVICE_DRIVING_URL);

        int i = 0, length = points.length;

        for (; i < length; i++) {
            CoordinatesDTO point = points[i];
            Double latitude = point.getLatitude(),
                   longitude = point.getLongitude();
            if (i != 0) {
                buildedUrl.append(";");
            }

            buildedUrl.append(latitude);
            buildedUrl.append(",");
            buildedUrl.append(longitude);
        }

        buildedUrl.append("?overview=false");

        return buildedUrl.toString();
    }
}
