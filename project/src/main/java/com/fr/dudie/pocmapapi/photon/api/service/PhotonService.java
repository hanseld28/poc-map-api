package com.fr.dudie.pocmapapi.photon.api.service;

import com.fr.dudie.pocmapapi.photon.api.config.PhotonClientAPI;
import com.fr.dudie.pocmapapi.photon.api.dto.FeatureCollectionPhotonDTO;
import org.springframework.stereotype.Service;

@Service
public class PhotonService {

    private final PhotonClientAPI photonClientAPI;


    public PhotonService() {
        this.photonClientAPI = PhotonClientAPI.getInstance();
    }

    public FeatureCollectionPhotonDTO typingSearch(String query, String limit) throws Exception {
        return photonClientAPI.searchTyping(query, limit)
                .orElseThrow(() -> new Exception("Ocorreu um erro e não foi possível buscar o endereço solicitado."));
    }
}
