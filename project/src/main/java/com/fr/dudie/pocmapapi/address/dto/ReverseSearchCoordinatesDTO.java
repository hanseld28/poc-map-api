package com.fr.dudie.pocmapapi.address.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReverseSearchCoordinatesDTO {

    private double latitude;
    private double longitude;

}
