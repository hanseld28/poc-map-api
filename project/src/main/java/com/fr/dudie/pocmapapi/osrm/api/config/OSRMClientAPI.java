package com.fr.dudie.pocmapapi.osrm.api.config;

public interface OSRMClientAPI {

   String DEFAULT_BASE_URL = "https://router.project-osrm.org/";
}
