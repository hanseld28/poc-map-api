package com.fr.dudie.pocmapapi.osrm.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class RouteCalcFullResponseDTO {

    private String code;
    private List<WaypointDTO> waipoints;
    private List<RouteDTO> routes;
}
