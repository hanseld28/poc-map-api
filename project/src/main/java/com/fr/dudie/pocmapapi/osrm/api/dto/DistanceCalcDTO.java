package com.fr.dudie.pocmapapi.osrm.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DistanceCalcDTO {

    private CoordinatesDTO pointA;
    private CoordinatesDTO pointB;
}
