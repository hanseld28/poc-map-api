package com.fr.dudie.pocmapapi.address.service;

import com.fr.dudie.pocmapapi.address.dto.QuerySearchDTO;
import com.fr.dudie.pocmapapi.address.dto.ReverseSearchCoordinatesDTO;
import com.fr.dudie.pocmapapi.nominatim.api.config.JsonNominatimClientAPI;
import fr.dudie.nominatim.client.JsonNominatimClient;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

import fr.dudie.nominatim.model.Address;

@Service
public class AddressService extends JsonNominatimClientAPI {

    private final JsonNominatimClient jsonNominatimClient;

    public AddressService() {
        this.jsonNominatimClient = JsonNominatimClientAPI.getInstance();
    }

    public List<Address> findAddressByQuery(QuerySearchDTO querySearchDTO) throws IOException {
        final List<Address> addresses = this.jsonNominatimClient.search(querySearchDTO.getQueryString());
        return addresses;
    }

    public Address findAddressByCoordinates(ReverseSearchCoordinatesDTO reverseSearchCoordinatesDTO) throws IOException {
        final Address address = this.jsonNominatimClient.getAddress(reverseSearchCoordinatesDTO.getLongitude(), reverseSearchCoordinatesDTO.getLatitude());
        return address;
    }


}
