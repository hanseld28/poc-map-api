package com.fr.dudie.pocmapapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocMapApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocMapApiApplication.class, args);
	}

}
