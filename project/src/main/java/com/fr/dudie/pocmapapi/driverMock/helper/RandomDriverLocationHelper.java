package com.fr.dudie.pocmapapi.driverMock.helper;

import com.fr.dudie.pocmapapi.driverMock.model.DriverMock;

import java.math.BigDecimal;
import java.util.List;

public class RandomDriverLocationHelper {

    private RandomDriverLocationHelper() { }

    public static List<DriverMock> randomizeDriversLocations(List<DriverMock> driversMockList) {
        int i = driversMockList.size() - 1;
        Double currentLatitude, currentLongitude, newLatitude, newLongitude;

        for (; i > -1; i--) {
            DriverMock driverMock = driversMockList.get(i);

            currentLatitude = driverMock.getLat();
            currentLongitude = driverMock.getLon();

            newLatitude = calculateNextNumber(currentLatitude);
            newLongitude = calculateNextNumber(currentLongitude);

            driverMock.setLat(newLatitude);
            driverMock.setLon(newLongitude);

            driversMockList.add(i, driverMock);
        }

        return driversMockList;
    }

    private static Double calculateNextNumber(Double number) {
        BigDecimal numberBased = new BigDecimal(number);
        BigDecimal numberReference = new BigDecimal(Math.random() * 0.77975737);
        BigDecimal result = numberBased.add(numberReference);

        return result.doubleValue();
    }
}
