package com.fr.dudie.pocmapapi.osrm.api.controller;

import com.fr.dudie.pocmapapi.osrm.api.dto.DistanceCalcDTO;
import com.fr.dudie.pocmapapi.osrm.api.dto.RouteCalcFullResponseDTO;
import com.fr.dudie.pocmapapi.osrm.api.dto.RouteDistance;
import com.fr.dudie.pocmapapi.osrm.api.service.OSRMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mapa/rota")
public class OSRMController {

    private final OSRMService osrmService;

    @Autowired
    public OSRMController(OSRMService osrmService) {
        this.osrmService = osrmService;
    }

    @PostMapping(value = "/distancia", consumes = "application/json", produces = "application/json")
    public RouteDistance getDistanceBetweenTwoPoints(@RequestBody DistanceCalcDTO distanceCalcDTO) {

        RouteDistance routeDistance = new RouteDistance();

        try {
            RouteCalcFullResponseDTO routeCalcFullResponseDTO = osrmService.getDistanceBetweenTwoPoints(distanceCalcDTO);

            Double distance = routeCalcFullResponseDTO.getRoutes().get(0).getDistance();
            routeDistance.setDistance(distance);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return routeDistance;
    }
}
