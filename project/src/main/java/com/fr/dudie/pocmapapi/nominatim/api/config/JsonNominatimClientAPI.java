package com.fr.dudie.pocmapapi.nominatim.api.config;

import fr.dudie.nominatim.client.JsonNominatimClient;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;

@Component
public class JsonNominatimClientAPI {

    private static JsonNominatimClient instanceOfJsonNominatimClient;

    public static JsonNominatimClient getInstance() {
        if (instanceOfJsonNominatimClient == null) {
            initialize();
        }

        return instanceOfJsonNominatimClient;
    }

    private static void initialize() {
        HttpClient httpClient = HttpClients.createDefault();
        String email = "hansel.donizete@newgo.com.br";
        instanceOfJsonNominatimClient = new JsonNominatimClient(httpClient, email);
    }

}
