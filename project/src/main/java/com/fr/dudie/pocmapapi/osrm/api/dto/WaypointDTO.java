package com.fr.dudie.pocmapapi.osrm.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class WaypointDTO {

    private String hint;
    private List<Double> location;
    private String name;
}
