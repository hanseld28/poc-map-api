package com.fr.dudie.pocmapapi.photon.api.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PropertiesDTO {

    @SerializedName(value = "osm_id")
    private Long osmId;

    @SerializedName(value = "osm_type")
    private String osmType;

    private List<Double> extent;
    private String country;

    @SerializedName(value = "osm_key")
    private String osmKey;

    @SerializedName(value = "osm_value")
    private String osmValue;
    private String name;
    private String state;

}
