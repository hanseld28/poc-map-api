package com.fr.dudie.pocmapapi.photon.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FeatureAddressPhotonDTO {

    private GeometryDTO geometry;
    private String type;
    private PropertiesDTO properties;

}
