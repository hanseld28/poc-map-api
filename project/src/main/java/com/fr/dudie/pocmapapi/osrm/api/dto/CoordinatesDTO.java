package com.fr.dudie.pocmapapi.osrm.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CoordinatesDTO {

    private Double latitude;
    private Double longitude;
}
