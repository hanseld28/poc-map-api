package com.fr.dudie.pocmapapi.photon.api.controller;

import com.fr.dudie.pocmapapi.photon.api.dto.FeatureCollectionPhotonDTO;
import com.fr.dudie.pocmapapi.photon.api.service.PhotonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mapa/typingSearch")
public class PhotonController {

    private final PhotonService photonService;

    @Autowired
    public PhotonController(PhotonService photonService) {
        this.photonService = photonService;
    }

    @GetMapping(produces = "application/json")
    public FeatureCollectionPhotonDTO doTypingSearch(@RequestParam(name = "q") String query, @RequestParam(name = "limit", required = false, defaultValue = "5") String limit) {
        FeatureCollectionPhotonDTO featureCollectionPhotonDTO = null;
        try {
            featureCollectionPhotonDTO = photonService.typingSearch(query, limit);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        return featureCollectionPhotonDTO;
    }

}
