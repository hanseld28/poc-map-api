package com.fr.dudie.pocmapapi.photon.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GeometryDTO {

    private List<Double> coordinates;
    private String type;

}
