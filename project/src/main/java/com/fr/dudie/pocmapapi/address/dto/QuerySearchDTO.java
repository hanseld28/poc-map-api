package com.fr.dudie.pocmapapi.address.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuerySearchDTO {

    private String queryString;
}
