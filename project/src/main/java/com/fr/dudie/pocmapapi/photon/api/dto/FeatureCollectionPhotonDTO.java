package com.fr.dudie.pocmapapi.photon.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FeatureCollectionPhotonDTO {

    private List<FeatureAddressPhotonDTO> features;
}
