package com.fr.dudie.pocmapapi.address.controller;

import com.fr.dudie.pocmapapi.address.dto.QuerySearchDTO;
import com.fr.dudie.pocmapapi.address.dto.ReverseSearchCoordinatesDTO;
import com.fr.dudie.pocmapapi.address.service.AddressService;
import fr.dudie.nominatim.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/mapa/enderecos")
public class AddressController {

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        super();
        this.addressService = addressService;
    }

    @PostMapping(value = "/busca/nomes", consumes = "application/json")
    public List<Address> searchAddressByQuery(@RequestBody QuerySearchDTO querySearchDTO) {
        List<Address> foundAddresses = null;

        try {
            foundAddresses = this.addressService.findAddressByQuery(querySearchDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return foundAddresses;
    }

    @PostMapping(value = "/busca/coordenadas", consumes = "application/json")
    public Address reverseSearchAddress(@RequestBody ReverseSearchCoordinatesDTO reverseSearchCoordinatesDTO) {
        Address foundAddress = null;

        try {
            foundAddress = this.addressService.findAddressByCoordinates(reverseSearchCoordinatesDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return foundAddress;
    }

}
