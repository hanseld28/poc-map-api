package com.fr.dudie.pocmapapi.photon.api.handler;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

public class PhotonResponseHandler<T> implements ResponseHandler<T> {

    private final Gson gsonInstance;
    private final Type responseType;

    public PhotonResponseHandler(Gson gsonInstance, Type responseType) {
        this.gsonInstance = gsonInstance;
        this.responseType = responseType;
    }

    @Override
    public T handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
        InputStream content = null;

        T object;

        try {
            StatusLine status = httpResponse.getStatusLine();
            if (status.getStatusCode() >= 400) {
                throw new IOException(String.format("HTTP error: %s %s", status.getStatusCode(), status.getReasonPhrase()));
            }

            content = httpResponse.getEntity().getContent();
            object = this.gsonInstance.fromJson(new InputStreamReader(content, "utf-8"), this.responseType);
        } finally {
            if (null != content) {
                content.close();
            }

            httpResponse.getEntity().consumeContent();
        }

        return object;
    }
}
