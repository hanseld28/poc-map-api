package com.fr.dudie.pocmapapi.driverMock.helper;

import com.fr.dudie.pocmapapi.driverMock.model.DriverMock;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DriverMockHelper {

    public static DriverMockHelper instance;
    private final Gson gson;
    private List<DriverMock> driversMockList;

    private DriverMockHelper() {
        super();
        this.gson = new Gson();
        this.processDriversLocationJsonFile();
    }

    public static DriverMockHelper getInstance() {
        if (instance == null) {
            instance = new DriverMockHelper();
        }
        return instance;
    }

    public List<DriverMock> findAllDrivers() {
        return this.driversMockList;
    }

    private void processDriversLocationJsonFile() {
        try {
            URI jsonFileURI = new ClassPathResource("static/assets/datasets/drivers_location.json").getURI();
            Reader reader = Files.newBufferedReader(Paths.get(jsonFileURI));
            TypeToken<List<DriverMock>> typeTokenListDrivers = new TypeToken<List<DriverMock>>() {};
            List<DriverMock> driversList = gson.fromJson(reader, typeTokenListDrivers.getType());

            loadDriversMockList(driversList);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private void loadDriversMockList(List<DriverMock> driversMockList) {
        this.driversMockList = driversMockList;
    }
}
