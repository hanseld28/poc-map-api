package com.fr.dudie.pocmapapi.photon.api.config;

import com.fr.dudie.pocmapapi.osrm.api.helper.ObjectElementDeserializer;
import com.fr.dudie.pocmapapi.photon.api.dto.FeatureCollectionPhotonDTO;
import com.fr.dudie.pocmapapi.photon.api.handler.PhotonResponseHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.dudie.nominatim.model.Element;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.HttpClients;

import java.net.URI;
import java.util.Optional;

public class PhotonClientAPI {
    private static PhotonClientAPI instance;
    private static final String TYPING_SEARCH_URL = "http://photon.komoot.de/api/";
    private final Gson gsonInstance;
    private final HttpClient httpClient;
    private PhotonResponseHandler<FeatureCollectionPhotonDTO> defaultSearchResponseHandler;


    private PhotonClientAPI(HttpClient httpClient) {
        this.httpClient = httpClient;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Element.class, new ObjectElementDeserializer());
        this.gsonInstance = gsonBuilder.create();
        this.defaultSearchResponseHandler = new PhotonResponseHandler<>(this.gsonInstance, (new TypeToken<FeatureCollectionPhotonDTO>() {
        }).getType());
    }

    public static PhotonClientAPI getInstance() {
        if (instance == null) {
            instance = new PhotonClientAPI(HttpClients.createDefault());
        }

        return instance;
    }

    public Optional<FeatureCollectionPhotonDTO> searchTyping(String query, String limit) {
        Optional<FeatureCollectionPhotonDTO> optionalFeatureCollectionPhotonDTO = null;

        try {
            String url = TYPING_SEARCH_URL + "?q=" + URLEncodedUtils.formatSegments(query) + "&limit=" + limit;
            System.out.println(url);
            URIBuilder uriBuilder = new URIBuilder(url);
            URI uri = uriBuilder.build();
            HttpGet getRequest = new HttpGet(uri);
            getRequest.addHeader("accept", "application/json");
            FeatureCollectionPhotonDTO featureCollectionPhotonDTO = httpClient.execute(getRequest, this.defaultSearchResponseHandler);

            optionalFeatureCollectionPhotonDTO = Optional.of(featureCollectionPhotonDTO);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return optionalFeatureCollectionPhotonDTO;
    }



}
