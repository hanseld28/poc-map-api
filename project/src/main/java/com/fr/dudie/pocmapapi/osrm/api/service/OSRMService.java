package com.fr.dudie.pocmapapi.osrm.api.service;

import com.fr.dudie.pocmapapi.osrm.api.config.RouteOSRMClientAPI;
import com.fr.dudie.pocmapapi.osrm.api.dto.CoordinatesDTO;
import com.fr.dudie.pocmapapi.osrm.api.dto.DistanceCalcDTO;
import com.fr.dudie.pocmapapi.osrm.api.dto.RouteCalcFullResponseDTO;
import org.springframework.stereotype.Service;

@Service
public class OSRMService {

    private final RouteOSRMClientAPI routeOSRMClientAPI;

    public OSRMService() {
        this.routeOSRMClientAPI = RouteOSRMClientAPI.getInstance();
    }

    public RouteCalcFullResponseDTO getDistanceBetweenTwoPoints(DistanceCalcDTO distanceCalcDTO) throws Exception {
        CoordinatesDTO pointA = distanceCalcDTO.getPointA();
        CoordinatesDTO pointB = distanceCalcDTO.getPointB();

        return routeOSRMClientAPI
                .getDistanceBetweenTwoPoints(pointA, pointB)
                .orElseThrow(() -> new Exception("Ocorreu um erro e não foi possível calcular a distância para essa rota."));

    }
}
