# PoC to Map API 

This project is a simple Proof of Concept (PoC) to test requirements of Map API. 

### Artifacts for this PoC

* [Open Street Map](https://wiki.openstreetmap.org/wiki/Main_Page) - This project is a base many other Map APIs. The project that creates and distributes free geographic data for the world.
* [Java implementation](https://github.com/jeremiehuchet/nominatim-java-api) of [Nominatim API](https://nominatim.org/release-docs/develop/api/Overview/) - Nominatim indexes named (or numbered) features within the OpenStreetMap (OSM) dataset and a subset of other unnamed features (pubs, hotels, churches, etc). The Java implementation already used offered two features: addresses search (by query or name) and reverse geocoding search (by latitude or longitude).
* [Leaflet.js](https://leafletjs.com/) - Frontend Map API. Offered support to many plugins to add new features to map.
* [OSRM - Route service](http://project-osrm.org/docs/v5.5.1/api/#route-service) - OSRM (Open Source Routing Machine) is a Modern C++ routing engine API for shortest paths in road networks. The route service that this API offers, finds the fastest route between coordinates in the supplied order.
* [Photon](https://photon.komoot.de/) - Photon is an open source geocoder built for OpenStreetMap data. It is based on elasticsearch - an efficient, powerful and highly scalable search platform. The feature used in this PoC is "search as you type". 


## PoC requirements

- [X] Allow the representation of points on the map (without limits) with different icons (no need for grouping - clusterization - the idea is to move it to the backend);
      
- [X] Allow to discover the current zoom level of the map, as well as the point (latitude, longitude);
      
- [X] Allow to draw a circle on the map.
   
- [X] Should must be possible to calculate the distance between two points on the map.

- [X] Should must be possible to pick a marker and move it around the map.

- [X] Should must be possible to remove individual markers by clicking on a marker.

- [X] Should must be possible to view the address options when typing in an address search.

- [ ] Allow movement on the map, generating new requests for the backend when moving (so we don't have to bring everything to the front);


### Screenshots

* Some concepts already tested

![Final experiment screenshot of PoC](/screeshots/final_experiment.png)